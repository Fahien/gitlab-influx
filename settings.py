import os

GITLAB_URL = os.environ.get('GITLAB_URL', 'https://gitlab.freedesktop.org')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN', '<personal access token from GitLab with api scope>')
GITLAB_PROJECT_PATH = os.environ.get('GITLAB_PROJECT_PATH', 'mesa/mesa')
MINIO_URL = os.environ.get('MINIO_URL', 'https://minio-packet.freedesktop.org')
INFLUX_URL= os.environ.get('INFLUX_URL', 'https://influx-packet.freedesktop.org')
INFLUX_ORG = os.environ.get('INFLUX_ORG', 'Collabora Ltd.')
INFLUX_BUCKET = os.environ.get('INFLUX_BUCKET', 'mesa')
INFLUX_TOKEN = os.environ.get('INFLUX_TOKEN', '<token from InfluxDB with bucket read/write access>')
